module.exports = {
  mode: "production",
  entry: "./src/js/main.js",
  output: {
    path: __dirname + "/build",
    filename: "bundle.js"
  },
  module: {
    rules: [{
      test: /\.s?css$/,
      use: [
        "style-loader", // creates style nodes from JS strings
        "css-loader", // translates CSS into CommonJS
        "sass-loader" // compiles Sass to CSS
      ]
    },
    {
      test: /\.html$/,
      use: 'raw-loader'
    }]
  },
  devtool: 'inline-source-map'
}
